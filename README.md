# React Diploma Project

This is a repository for React version of "Delivery control" page.

## Build Setup

``` bash
# npm install
install dependencies

# npm start
Runs the app in the development mode
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# npm test
Launches the test runner in the interactive watch mode.

# npm run build
Builds the app for production to the `build` folder.
```