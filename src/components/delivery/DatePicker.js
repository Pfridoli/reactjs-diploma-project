import React from "react";
import DateRangePicker from "react-daterange-picker";
import "react-daterange-picker/dist/css/react-calendar.css";
import originalMoment from "moment";
import {extendMoment} from "moment-range";

const moment = extendMoment(originalMoment);

class DatePicker extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            value: moment.range(moment(new Date()).startOf('month'), moment(new Date()).endOf('month'))
        };
        this.props.onUserInput(this.state.value);
    }

    onSelect = (value, states) => {
        this.setState({value, states}, () => {
            this.props.onUserInput(this.state.value)
        });
    };

    changeDate = (event, pickedDate) => {
        let newDateRange = '';
        switch (pickedDate) {
            case 'newToday':
                newDateRange = moment.range(moment(new Date()).startOf('day'), moment(new Date()).endOf('day'));
                this.setState({value: newDateRange}, () => {
                    this.props.onUserInput(newDateRange)
                });
                break;
            case 'newYesterday':
                newDateRange = moment.range(moment(new Date()).subtract(1, 'days').startOf('day'),
                    moment(new Date()).subtract(1, 'days').endOf('day'));
                this.setState({value: newDateRange}, () => {
                    this.props.onUserInput(newDateRange)
                });
                break;
            case 'newLMonth':
                newDateRange = moment.range(moment(new Date()).subtract(1, 'months').startOf('month'),
                    moment(new Date()).subtract(1, 'months').endOf('month'));
                this.setState({value: newDateRange}, () => {
                    this.props.onUserInput(newDateRange)
                });
                break;
            case 'newTMonth':
                newDateRange = moment.range(moment(new Date()).startOf('month'), moment(new Date()).endOf('month'));
                this.setState({value: newDateRange}, () => {
                    this.props.onUserInput(newDateRange)
                });
                break;
            default:
                break;
        }
    };

    render() {
        return (
            <div className="calendar-body d-flex flex-column">
                <div className="calendar-body-filter row mb-4">
                    <div className="col-3">
                        <button onClick={(event) => this.changeDate(event, 'newToday')}>
                            {'Today'}
                        </button>
                    </div>
                    <div className="col-3">
                        <button onClick={(event) => this.changeDate(event, 'newYesterday')}>
                            {'Yesterday'}
                        </button>
                    </div>
                    <div className="col-3">
                        <button onClick={(event) => this.changeDate(event, 'newTMonth')}>
                            {'This month'}
                        </button>
                    </div>
                    <div className="col-3">
                        <button onClick={(event) => this.changeDate(event, 'newLMonth')}>
                            {'Last month'}
                        </button>
                    </div>
                </div>
                <div className="row border-between">
                    <div className="dropdown col-6 text-right">
                        <input className="form-control" type="text" name="firstDate"
                               value={this.state.value.start.format("DD.MM.YYYY")} readOnly/>
                        <div className="dropdown-content">
                            <DateRangePicker
                                value={this.state.value}
                                onSelect={this.onSelect}
                                selectionType={'range'}
                                numberOfCalendars={2}
                            />
                        </div>
                    </div>
                    <div className="dropdown col-6 text-left">
                        <input className="form-control" type="text" name="secondDate"
                               value={this.state.value.end.format("DD.MM.YYYY")} readOnly/>
                        <div className="dropdown-content">
                            <DateRangePicker
                                value={this.state.value}
                                onSelect={this.onSelect}
                                selectionType={'range'}
                                numberOfCalendars={2}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DatePicker;
