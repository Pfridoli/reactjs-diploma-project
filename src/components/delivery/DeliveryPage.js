/**
 Took inspiration from https://codepen.io/Shamiul_Hoque/pen/LNavdZ
 **/
import * as React from "react";
import Select from 'react-select';
import * as Datetime from 'react-datetime';
import DatePicker from './DatePicker';
import {extendMoment} from "moment-range";
import originalMoment from "moment";

const moment = extendMoment(originalMoment);

const preventions = [
    {id: 1, value: 'Employees were trained', label: 'Employees were trained'},
    {id: 2, value: 'Guidance was modified', label: 'Guidance was modified'},
    {id: 3, value: 'Returned goods', label: 'Returned goods'},
    {id: 4, value: 'Food was reprocessed', label: 'Food was reprocessed'},
    {id: 5, value: 'Food was rejected', label: 'Food was rejected'},
    {id: 6, value: 'Supplier was notified', label: 'Supplier was notified'}
];

const defects = [
    {id: 1, value: 'Expiration date exceeded', label: 'Expiration date exceeded'},
    {id: 2, value: 'Product is not quality', label: 'Product is not quality'},
    {id: 3, value: 'Package was broken', label: 'Package was broken'},
    {id: 4, value: 'Temperature was not correct', label: 'Temperature was not correct'}
];

class DeliveryPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};
        this.state.selectedTableDate = "";
        this.state.deliveries = [];

        window.addEventListener('beforeunload', this.refreshPageHandler);
    }

    componentDidMount() {
        this.hydrateStateWithLocalStorage();
        window.addEventListener(
            "beforeunload",
            this.saveStateToLocalStorage.bind(this)
        );
    }

    componentWillUnmount() {
        window.removeEventListener(
            "beforeunload",
            this.saveStateToLocalStorage.bind(this)
        );
        this.saveStateToLocalStorage();
    }

    hydrateStateWithLocalStorage() {
        if (localStorage.getItem('linesStorage')) {
            try {
                this.setState({deliveries: JSON.parse(localStorage.getItem('linesStorage'))});
            } catch (e) {
                localStorage.removeItem('linesStorage');
            }
        }
    }

    saveStateToLocalStorage() {
        if (this.state.deliveries !== undefined && this.state.deliveries !== 0) {
            let deliveries = this.state.deliveries.slice()
            if (JSON.parse(localStorage.getItem('linesStorage'))) {
                let oldLines = JSON.parse(localStorage.getItem('linesStorage'));
                if (oldLines !== deliveries) {
                    localStorage.setItem('linesStorage', JSON.stringify(deliveries));
                }
            } else {
                localStorage.setItem('linesStorage', JSON.stringify(deliveries));
            }
        }
    }

    getBase64FromDataURL(dataURL) {
        const indexOfBase64Code = dataURL.indexOf('base64') + 7;
        return dataURL.slice(indexOfBase64Code);
    }

    checkImage(event, data) {
        if (event.target.files && event.target.files.length > 0) {
            const file = event.target.files[0];

            if (
                file.type !== 'image/jpeg' &&
                file.type !== 'image/png' &&
                file.type !== 'image/jpg' &&
                file.type !== 'image/gif'
            ) {
                return;
            }

            if (file.size > 5240000) {
                return;
            }
            const fileReader = new FileReader();

            fileReader.addEventListener('load', () => {
                return this.addPicture(file.name, this.getBase64FromDataURL(fileReader.result), data);
            });
            fileReader.readAsDataURL(file);
            event.target.value = null;
        }
    }

    addPicture(picName, picData, data) {
        if (picName !== null && picData !== null) {
            let picObject = {
                certificate_file: picData,
                certificate_file_name: picName
            };
            let item = {
                id: data.id,
                name: data.name,
                value: picObject
            };
            this.addDataToDelivery(item)
        } else {
            return null;
        }
    }

    showPicture(picData) {
        const binary = this.fixBinary(atob(picData.picture.certificate_file));
        const fileURL = URL.createObjectURL(new Blob([binary]));
        const a = document.createElement('a');
        a.href = fileURL;
        a.download = picData.picture.certificate_file_name;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    fixBinary(bin) {
        let length = bin.length;
        let buf = new ArrayBuffer(length);
        let arr = new Uint8Array(buf);
        for (let i = 0; i < length; i++) {
            arr[i] = bin.charCodeAt(i);
        }
        return buf;
    }

    handleDeliveryTable(event, data) {
        let item = null;
        switch (data.name) {
            case 'supplier':
            case 'product':
            case 'temp':
                item = {
                    id: event.target.id,
                    name: event.target.name,
                    value: event.target.value
                };
                this.addDataToDelivery(item);
                break;
            case 'time':
            case 'defect':
            case 'prevent':
                item = {
                    id: data.id,
                    name: data.name,
                    value: event
                };
                this.addDataToDelivery(item);
                break;
            case 'pictures':
                this.checkImage(event, data);
                break;
            case 'getPicture':
                this.showPicture(data);
                break;
            case 'deletePicture':
                console.log(data)
                let deliveries = this.state.deliveries.slice();
                let newValue = deliveries.map(delivery => {
                    if (delivery.id === data.picturesListId) {
                        return delivery.pictures.splice(data.pictureIndex, 1)
                    }
                });

                item = {
                    id: data.picturesListId,
                    name: data.name,
                    value: newValue
                };
                this.addDataToDelivery(item);
                break;
            default:
                break;
        }
    };

    addDataToDelivery(item) {
        if (item) {
            let deliveries = this.state.deliveries.slice();
            let newDeliveries = deliveries.map(delivery => {
                for (let key in delivery) {
                    if (delivery.hasOwnProperty(key)) {
                        if (key === item.name && delivery.id === item.id) {
                            if (item.name !== 'pictures') {
                                delivery[key] = item.value;
                            } else {
                                let pictures = delivery[key];
                                pictures.push(item.value);
                                delivery[key] = pictures;
                            }
                        }
                    }
                }
                return delivery
            });
            this.setState({deliveries: newDeliveries});
        }
    }

    handleAddEvent(event) {
        let id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
        let delivery = {
            id: id,
            time: moment(new Date()),
            supplier: '',
            product: '',
            temp: '',
            defect: [],
            prevent: [],
            pictures: [],
            isOpen: false
        };
        this.state.deliveries.push(delivery);
        this.setState(this.state.deliveries);
    }

    handleUserInput(selectedTableDate) {
        this.setState({selectedTableDate: selectedTableDate})
    }

    handleDeleteRow(delivery) {
        let index = this.state.deliveries.indexOf(delivery);
        this.state.deliveries.splice(index, 1);
        this.setState(this.state.deliveries)
    }

    handleOpenPictureRow(event, deliveryiId) {
        let newDeliveries = this.state.deliveries.slice();

        newDeliveries.map(delivery => {
            if (delivery.id !== deliveryiId) {
                delivery.isOpen = false
            } else {
                delivery.isOpen = !delivery.isOpen
            }
        });

        this.setState({deliveries: newDeliveries});
    }

    render() {
        return (
            <div className="container monitoring__sheet">
                <div className="row d-flex align-items-center justify-content-center">
                    <p className="h5 font-weight-bold text-uppercase text-center">
                        Delivery control REACT
                    </p>
                </div>
                <div className="row d-flex align-items-center justify-content-center">
                    <DatePicker selectedTableDate={this.state.selectedTableDate}
                                onUserInput={this.handleUserInput.bind(this)}/>
                </div>
                <div className="row d-flex align-items-center justify-content-center">
                    <DeliveryTable onDeliveryTableUpdate={this.handleDeliveryTable.bind(this)}
                                   onRowAdd={this.handleAddEvent.bind(this)}
                                   onRowDeletion={this.handleDeleteRow.bind(this)}
                                   deliveries={this.state.deliveries}
                                   selectedTableDate={this.state.selectedTableDate}
                                   onRowOpenPicture={this.handleOpenPictureRow.bind(this)}/>
                </div>
            </div>
        );
    }
}

class DeliveryTable extends React.Component {

    render() {
        let onDeliveryTableUpdate = this.props.onDeliveryTableUpdate;
        let rowDelete = this.props.onRowDeletion;
        let onRowOpenPicture = this.props.onRowOpenPicture;
        let selectedTableDate = this.props.selectedTableDate;
        let delivery = [];
        let emptyDelivery = null
        if (this.props.deliveries && this.props.deliveries.length !== 0) {
            emptyDelivery = null
            delivery = this.props.deliveries.map(delivery => {
                if (selectedTableDate.contains(moment(delivery.time))) {
                    return (<DeliveryRow onDeliveryTableUpdate={onDeliveryTableUpdate} delivery={delivery}
                                         onDelEvent={rowDelete.bind(this)} onRowOpenPicture={onRowOpenPicture}
                                         key={delivery.id}/>)
                }
            });
        } else {
            delivery = [];
            emptyDelivery = <EmptyDeliveryRow/>
        }
        return (
            <div className="container-fluid deliveryPage">
                <div className="row mb-3">
                    <div className="col-12">
                        <button type="button" onClick={this.props.onRowAdd}
                                className="btn btn-success float-right">New row
                        </button>
                    </div>
                </div>
                <div className="row deliveryTable">
                    <div className="col-12">
                        <table className="table">
                            <thead className="tableHead">
                            <tr>
                                <th scope="col" className="time">Time</th>
                                <th scope="col" className="supplier">Supplier</th>
                                <th scope="col" className="product">Product</th>
                                <th scope="col" className="temp">Product Temp</th>
                                <th scope="col" className="defect">Defects</th>
                                <th scope="col" className="response">What I did</th>
                                <th scope="col" className="photo"/>
                                <th scope="col" className="delete"/>
                            </tr>
                            </thead>
                            <tbody className="tableBody">
                            {delivery}
                            </tbody>
                        </table>
                        {emptyDelivery}
                    </div>
                </div>
            </div>
        )
    }
}

class EmptyDeliveryRow extends React.Component {
    render() {
        return (
            <div>
                <div className="text-left">
                    No Data to show.
                </div>
                <div className="text-left">
                    Click <span className="font-bold">New row</span> to add deliveries.
                </div>
            </div>
        )
    }
}

class DeliveryRow extends React.Component {
    onDelEvent() {
        this.props.onDelEvent(this.props.delivery);
    }

    render() {
        let onRowOpenPicture = this.props.onRowOpenPicture;

        return (
            <tr>
                <EditableCalendarCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate} cellData={{
                    name: "time",
                    value: this.props.delivery.time,
                    id: this.props.delivery.id
                }}/>
                <EditableInputCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate} cellData={{
                    name: "supplier",
                    value: this.props.delivery.supplier,
                    id: this.props.delivery.id,
                    type: "text"
                }}/>
                <EditableInputCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate} cellData={{
                    name: "product",
                    value: this.props.delivery.product,
                    id: this.props.delivery.id,
                    type: "text"
                }}/>
                <EditableTempInputCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate} cellData={{
                    name: "temp",
                    value: this.props.delivery.temp,
                    id: this.props.delivery.id,
                    type: "number"
                }}/>
                <EditableSelectCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate} cellData={{
                    name: "defect",
                    value: this.props.delivery.defect,
                    id: this.props.delivery.id,
                    data: preventions
                }}
                />
                <EditableSelectCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate} cellData={{
                    name: "prevent",
                    value: this.props.delivery.prevent,
                    id: this.props.delivery.id,
                    data: defects
                }}/>
                <EditablePictureCell onDeliveryTableUpdate={this.props.onDeliveryTableUpdate}
                                     onEventRowOpenPicture={onRowOpenPicture.bind(this)}
                                     cellData={{
                                         name: "pictures",
                                         value: this.props.delivery.pictures,
                                         id: this.props.delivery.id,
                                         isOpen: this.props.delivery.isOpen
                                     }}
                />
                <td className="tableDeleteRow">
                    <i className="fas fa-times" onClick={this.onDelEvent.bind(this)}/>
                </td>
            </tr>
        )
    }
}

class EditableCalendarCell extends React.Component {
    render() {
        return (
            <td>
                <Datetime value={moment(this.props.cellData.value).format('DD.MM.YYYY h:mm')}
                          id={this.props.cellData.id}
                          name={this.props.cellData.name}
                          onChange={(e) => this.props.onDeliveryTableUpdate(e, {
                              id: this.props.cellData.id,
                              name: this.props.cellData.name
                          })}/>
            </td>
        );
    }
}

class EditableInputCell extends React.Component {
    render() {
        return (
            <td>
                <div className="input-group">
                    <input type={this.props.cellData.type}
                           className="form-control"
                           name={this.props.cellData.name}
                           id={this.props.cellData.id}
                           value={this.props.cellData.value}
                           onChange={(e) => this.props.onDeliveryTableUpdate(e, {
                               id: this.props.cellData.id,
                               name: this.props.cellData.name
                           })}/>
                </div>
            </td>
        );
    }
}

class EditableTempInputCell extends React.Component {
    render() {
        return (
            <td>
                <div className="input-group align-items-center">
                    <input type={this.props.cellData.type}
                           className="form-control"
                           name={this.props.cellData.name}
                           id={this.props.cellData.id}
                           value={this.props.cellData.value}
                           onChange={(e) => this.props.onDeliveryTableUpdate(e, {
                               id: this.props.cellData.id,
                               name: this.props.cellData.name
                           })}/>°C
                </div>
            </td>
        );
    }
}

class EditableSelectCell extends React.Component {
    render() {
        return (
            <td>
                <Select value={this.props.cellData.value}
                        isMulti={true}
                        name={this.props.cellData.name}
                        onChange={(e) => this.props.onDeliveryTableUpdate(e, {
                            id: this.props.cellData.id,
                            name: this.props.cellData.name
                        })}
                        options={this.props.cellData.data}
                        id={this.props.cellData.id}/>
            </td>
        );
    }
}

class EditablePictureCell extends React.Component {
    onEventRowOpenPicture(event) {
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }
        this.props.onEventRowOpenPicture(event, this.props.cellData.id);
    }

    render() {
        let pictures = [];
        if (this.props.cellData.value) {
            let index = -1;
            this.props.cellData.value.map(picture => {
                index++;
                let id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
                pictures.push(<PicturesList onDeliveryTableUpdate={this.props.onDeliveryTableUpdate}
                                            picture={picture} key={id} pictureId={id} pictureIndex={index}
                                            picturesListId={this.props.cellData.id}/>)
            });
        }

        return (
            <td>
                <div
                    className={`picture-popup green d-block cursor-pointer text-center align-middle position-relative 
                    ${this.props.cellData.isOpen ? 'open' : ''}`}>
                    <div
                        className={`picture-hidden d-flex position-absolute ${this.props.cellData.isOpen ? 'show' : ''}`}>
                        <div className="inner d-flex flex-column text-left">
                            {pictures}
                            <div className="upload-btn-wrapper mt-auto">
                                <label
                                    className="d-flex text-right font-weight-bold green bg-transparent p-0 font-16 m-0">
                                    <input
                                        name={this.props.cellData.name}
                                        id={this.props.cellData.id}
                                        type="file"
                                        ref="file"
                                        accept=".jpeg, .png, .jpg, .gif"
                                        onChange={(e) => this.props.onDeliveryTableUpdate(e, {
                                            id: this.props.cellData.id,
                                            name: this.props.cellData.name
                                        })}/>
                                    <i className="fas fa-plus"/>
                                    <span className="pl-1">{'Add picture'}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex flex-row font-20 align-middle"
                         onClick={this.onEventRowOpenPicture.bind(this)}>
                        <i className="fas fa-camera position-relative"/>
                        <div>
                            {(!this.props.cellData.isOpen && this.props.cellData.value !== null && this.props.cellData.value.length !== 0) ?
                                <div
                                    className="counter green font-weight-bold ml-1">{this.props.cellData.value.length}</div> :
                                <div/>
                            }
                        </div>
                    </div>
                </div>
            </td>
        );
    }
}

class PicturesList extends React.Component {
    render() {
        return (
            <div>
                <div className="picture-download-name" id={this.props.pictureId}>
                        <span className="text-underline font-15 black cursor-pointer"
                              onClick={(e) => this.props.onDeliveryTableUpdate(e, {
                                  picture: this.props.picture,
                                  name: 'getPicture',

                              })}>
                            {this.props.picture.certificate_file_name}
                    </span>
                    <span className="font-15 red cursor-pointer ml-1"
                          onClick={(e) => this.props.onDeliveryTableUpdate(e, {
                              name: 'deletePicture',
                              pictureIndex: this.props.pictureIndex,
                              picturesListId: this.props.picturesListId
                          })}
                    >
                    <i className="fas fa-times"/>
                </span>
                </div>
            </div>
        )
    }
}

export default DeliveryPage;
