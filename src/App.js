import React, {Component} from 'react';
import './assets/styles/App.scss';
import './assets/styles/component-styles/react-datetime.scss';
import DeliveryPage from './components/delivery/DeliveryPage';

class App extends Component {
    render() {
        return (
            <div className="App">
                <DeliveryPage/>
            </div>
        );
    }
}

export default App;
